﻿#include "IterativeRePair.hpp"

int main(int argc, char* argv[]){ 
	if (argc == 2 && (std::string(argv[1]) == "--help" || std::string(argv[1]) == "-h")){
		std::cout << "Usage: RePairMetric [MODE] [INPUTFILE] [OUTPUTFILE]" << std::endl <<
				"       MODE       -> cX for compression mode with 2^X iterations, X is in range 0 to 9" << std::endl <<
				"                     dX for decompression mode, with X = 1 for slp decompression and X = 2 for default decompression" << std::endl <<
				"                     a for analysis mode, only an input file is needed" << std::endl <<
				"       INPUTFILE  -> path to sourcefile" << std::endl <<
				"       OUTPUTFILE -> path to targetfile" << std::endl;
		return -1;
	} else if (argc == 3 && argv[1][0] == 'a'){
		std::string inputfile(argv[2]);
		analyse(inputfile);
		return 0;
	} else if (argc != 4){
		std::cout << "Usage: RePairMetric [MODE] [INPUTFILE] [OUTPUTFILE]" << std::endl <<
				"Use RePairMetric --help or RePairMetric -h for help" << std::endl;
		return -1;
	}

	int result = 0;
	std::string inputfile(argv[2]);
	std::string outputfile(argv[3]);

	if (argv[1][0] == 'c'){ // compress mode
		uint16_t exp = argv[1][1] - '0';
		if(exp >= 0 && exp < 10){
			uint16_t iterations = 1 << (argv[1][1] - '0');
			result = compress(inputfile, outputfile, iterations);
		} else {
			std::cout << "illegal number of iterations" << std::endl;
			result = -1;
		}
	}

	if (argv[1][0] == 'd'){ // decompress mode
		uint8_t mode = (argv[1][1] - '0');
		if(mode == 1 || mode == 2){
			result = decompress(inputfile, outputfile, mode);
		} else {
			std::cout << "illegal decompression mode" << std::endl;
			result = -1;
		}
	}

	return result;
}

void analyse(std::string inputfile){
	std::cout << "Sequence-Analyser" << std::endl;
	Grammar *grammar = new Grammar(readFileIntoString(inputfile));
	Stats *stats = new Stats();

	std::cout << "Building grammar" << std::endl;

	stats->analyse(grammar->getAxiomPtr());
	//stats->printPairs();
	std::cout << "Sequence-Analyser: analyse done" << std::endl;
	delete grammar;

	std::cout << "Sequence-Analyser: grammar deleted" << std::endl;
	delete stats;

	std::cout << "Sequence-Analyser: stats deleted" << std::endl;
}

int compress(std::string inputfile, std::string outputfile, uint16_t iterations){
	std::cout << "Sequence-Analyser" << std::endl;
	int result = 0;
	Grammar *grammar = new Grammar(readFileIntoString(inputfile));
	Stats *stats = new Stats();
	stats->setIterations(iterations);

	std::cout << "Building grammar" << std::endl;

	int i;

	for (i = 0; i < iterations; ++i){
		//std::cout << "Iteration " << i+1 << " / " << iterations << std::endl;
		stats->setCurrentIteration(i);
		result = stats->compress(grammar);
		if (result == 1){
			break;
		}
	}

	//std::std::cout << grammar->toString();
	result = grammar->writeToCompressedFile(outputfile);
	if(result == 0){
		std::cout << "Rules built: " << grammar->getNumberOfRules() << " Iterations: " << i << std::endl;
	} else {
		std::cout << "No compression could be achieved. No output file is written." << std::endl;
	}

	delete grammar;

	delete stats;

	return result;
}

int decompress(std::string inputfile, std::string outputfile, uint8_t mode){
	std::cout << "decompression mode" << std::endl;
	if(inputfile.size() == 0 && outputfile.size() == 0){return -1;}
	int result = -1;

	std::cout << "read file... " << std::flush;
	std::string filecontent = readFileIntoString(inputfile);
	std::cout << "done" << std::endl;

	uint8_t seperator = filecontent[0];
	//std::cout << "seperator: " << +seperator << std::endl;
	uint8_t sizeNt = filecontent[1];
	//std::cout << "sizeNT: " << +sizeNt << std::endl;
	uint8_t sizeTarget = filecontent[2];
	//std::cout << "sizeTarget: " << +sizeTarget << std::endl;

	//std::cin.get();

	//skiping linefeed inserted by compression
	uint64_t index = 4;
	std::string axiom = "";

	//extract compressed axiom from filecontent
	std::cout << "read axiom... " << std::flush;
	auto filesize = filecontent.size();
	while ((uint8_t)filecontent[index] != seperator){
		//std::cout << index << " / " << filesize << ": " << +(uint8_t)filecontent[index] << std::endl;
		// if (index > 599203 - 30 && index < 599203 + 2){
		// 	std::cout << index << " / " << filesize << ": " << +(uint8_t)filecontent[index] << std::endl;
		// }
		axiom.push_back(filecontent[index]);
		++index;
	}
	std::cout << "done" << std::endl;

	if ((uint8_t)filecontent[index] == seperator){++index;}

	Grammar* cfg = new Grammar(axiom);
	Stats* stats = new Stats();

	//std::cout << "After axiom:" << index << " / " << filesize << ": " << +(uint8_t)filecontent[index] << std::endl;

	//extract rules
	int rules = 0;
	while (index + sizeTarget <= filesize){
		std::string nt = "";
		//std::cout << "NT : " << std::endl;
		for ( int i = 0; i < sizeNt; ++i){
				//std::cout << index << " / " << filesize << ": " << +(uint8_t)filecontent[index] << std::endl;
				nt.push_back(filecontent[index++]);
			}
		std::string target = "";
		//std::cout << "Target : " << std::endl;
		for ( int i = 0; i < sizeTarget; ++i){
				//std::cout << index << " / " << filesize << ": " << +(uint8_t)filecontent[index] << " " << index + sizeTarget << std::endl;
				target.push_back(filecontent[index++]);
				}

		//std::cout << "Rules done" << std::endl;
		cfg->addRule(new Rule(nt, target, 0));
		++rules;
		//std::cout << "Rules written" << std::endl;
	}
	std::cout << rules << "Rules found" << std::endl;

	std::cout << "Start decompress... " << std::endl;
	result = stats->decompress(cfg, mode);
	if(result == 0){cfg->writeToDecompressedFile(outputfile);}
	std::cout << "done" << std::endl;

	return result;
}

std::string readFileIntoString(const std::string& path) {
    std::ifstream input_file(path);
    if (!input_file.is_open()) {
        std::cerr << "Could not open the file - '"
             << path << "'" << std::endl;
        exit(EXIT_FAILURE);
    }
    return std::string((std::istreambuf_iterator<char>(input_file)), std::istreambuf_iterator<char>());
}
