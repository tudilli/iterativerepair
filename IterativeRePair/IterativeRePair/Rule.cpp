#include "Rule.hpp"

Rule::Rule(std::string nonterminal, std::string target, uint64_t uses){
    //std::cout << "Rule object creating" << std::endl;
    nonterminal_ = nonterminal;
    target_ = target;
    uses_ = uses;
    improvement_ = calculateImprovement();
    //std::cout << "Rule object created" << std::endl;
}

Rule::~Rule(){
}

std::string Rule::getTarget(){
    return target_;
}

std::string* Rule::getTargetPtr(){
    return &target_;
}

std::string Rule::getNonterminal(){
    return nonterminal_;
}

int64_t Rule::calculateImprovement(){
    // calculating the formula x(|t|-|n|) - |n| - |t| - |\rightarrow|
    return uses_ * (target_.length() - nonterminal_.length()) - nonterminal_.length() - target_.length() - 1;
}
