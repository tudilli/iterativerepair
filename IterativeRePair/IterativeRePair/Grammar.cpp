#include "Grammar.hpp"
#include <iostream>

Grammar::Grammar(std::string axiom){
    axiom_ = axiom;
    ruleseperator_ = 0-1;
    rules_ = new std::map<std::string, Rule*>();
}

Grammar::~Grammar(){
    for (auto itr = rules_->begin(); itr != rules_->end(); ++itr) {
        delete itr->second;
    }
    delete rules_;
}

void Grammar::addRule(Rule* rule){
    rules_->insert(make_pair(rule->getNonterminal() , rule));
}

void Grammar::deleteRule(std::string key){
    rules_->erase(key);
}

void Grammar::setAxiom(std::string axiom){
    axiom_ = axiom;
}

void Grammar::setRuleseperator(uint8_t seperator){
    ruleseperator_ = seperator;
}

std::string* Grammar::getAxiomPtr(){
    return &axiom_;
}

std::map<std::string, Rule*>* Grammar::getRulesPtr(){
    return rules_;
}

uint64_t Grammar::getNumberOfRules(){
    return rules_->size();
}

void Grammar::cleanupAxiom(const char placeholder){
    //std::cout << "Cleanup axiom...";
    std::string tmp = "";
    for(std::size_t itr = 0; itr < axiom_.size(); ++itr){
        if(axiom_[itr] != placeholder){
            //axiom_.erase(itr, 1);
            tmp.push_back(axiom_[itr]);
        }
    }
    setAxiom(tmp);
    //std::cout << " done" << std::endl;
}

void Grammar::convertToSLP(){
    int rulecount = 1;
    for (auto itr = rules_->begin(); itr != rules_->end(); ++itr){
        std::cout << "process rule " << rulecount++ << " / " << rules_->size() << "\r" << std::endl;
        auto target = itr->second->getTargetPtr();
        //uint64_t size = target->size();
        uint64_t pos = 0;
        while (pos < target->size()){
            //std::cout << "process pos " << pos << std::endl;
            std::string current = "";
            current.push_back((*target)[pos]);
            auto rule = rules_->find(current);
            if(rule != rules_->end() ){
                target->replace(pos, rule->first.length(), rule->second->getTarget());
                //size = size + rule->second->getTargetPtr()->length();
            } else {
                ++pos;
            }
        }
    }
}

std::string Grammar::toString(){
    std::string str = "Axiom: " + axiom_ + "\n";

    for(auto itr = rules_->begin(); itr != rules_->end(); ++itr){
        str = str + itr->second->getNonterminal() + "->" + itr->second->getTarget() + "\n";
    }

    return str;
}

std::string Grammar::rulesToString(){
    std::string str = "";

    for(auto itr = rules_->begin(); itr != rules_->end(); ++itr){
        str = str + itr->second->getNonterminal() + "->" + itr->second->getTarget() + "\n";
    }

    return str;
}

int Grammar::writeToCompressedFile(const std::string& path){
    std::ofstream output(path);

    if(rules_->size() != 0){
        uint8_t sizeNt = rules_->begin()->second->getNonterminal().size();
        uint8_t sizeTarget = rules_->begin()->second->getTarget().size();
        //writing header
        std::string seperator = "";
        seperator.push_back(ruleseperator_);
        std::string header = "";
        header.push_back(ruleseperator_);
        header.push_back(sizeNt);
        header.push_back(sizeTarget);
        output << header << "\n";

        //writing axiom
        output << "" + axiom_;
        output << ("" + seperator);

        //writing rules
        for(auto itr = rules_->begin(); itr != rules_->end(); ++itr){
            output << itr->second->getNonterminal() + itr->second->getTarget();
        }
    } else {
        return 1;
    }

    return 0;
}

int Grammar::writeToDecompressedFile(const std::string& path){
    std::ofstream output(path);

    output << "" + axiom_;

    return 0;
}
