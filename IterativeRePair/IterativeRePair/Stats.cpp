#include "Stats.hpp"
#include <iostream>

Stats::Stats(){
    alphabet_ = new std::set<uint8_t>();
    nonterminals_ = new std::vector<uint8_t>();
    nonterminalsSet_ = false;
    nextNt_ = 0;
    redundantPairs_ = new PairsContainer(0xffff);
    //blockedIndecies_ = new std::set<uint64_t>(); //TODO change data structure
    placeholder_ = '.';
    iterations_ = 0;
    currentIteration_ = 0;
    changes = true;
}

Stats::~Stats(){
    delete alphabet_;
    delete nonterminals_;
    delete redundantPairs_;
    //delete blockedIndecies_;
}

int Stats::compress(Grammar* cfg){
    
    if(changes){analyse(cfg->getAxiomPtr());}
    
    if(!nonterminalsSet_){
        populateNonTerminals();
        cfg->setRuleseperator(placeholder_);
        if(getnextNt()){ placeholder_ = nextNt_; }
    }
    //std::cout << "available NTs: " << nonterminals_->size() << std::endl;
    
    if(nonterminals_->size() == 0){
        std::cout << "no NTs left" << std::endl;
        return 1;
    } 
    if(redundantPairs_->size() == 0){
        std::cout << "no redundand pairs left" << std::endl;
        return 1;
    }
    
    //std::cout << "Replace pairs..." << std::endl;
    replaceredundantPairs(cfg);

    if(changes){
        cfg->cleanupAxiom(placeholder_); 
        
        //--Reset der RedundantPairs
        delete redundantPairs_;
        redundantPairs_ = new PairsContainer(0xffff);
    }
    return 0;
}

int Stats::decompress(Grammar* cfg, uint8_t mode){
    auto rules = cfg->getRulesPtr();
    for (auto itr = rules->cbegin(); itr != rules->cend(); ++itr){
        auto nt = (uint8_t) itr->second->getNonterminal().at(0);
        nonterminals_->push_back(nt);
    }
    if (mode == 1){
        //std::cout << "convert to SLP\r" << std::endl;
        cfg->convertToSLP();
        //std::cout << "convert to SLP... done" << std::endl;
    } 

    auto axiom = cfg->getAxiomPtr();
    //uint64_t size = axiom->size();
    uint64_t pos = 0;
    //std::cout << cfg->rulesToString() << std::endl;
    //std::cout << "Start replacement" << std::endl;
    while (pos < axiom->size()){
        //std::cout << "Next char: " << pos << " - " << axiom[pos] << std::endl;
        std::string current = "";
        current.push_back((*axiom)[pos]);
        auto rule = rules->find(current);
        if(rule != rules->end() ){
            //std::cout << "Replace: " << pos << std::endl;
            axiom->replace(pos, rule->first.length(), rule->second->getTarget());
            //size = size + rule->second->getTargetPtr()->length();
        } else {
            //std::cout << "skip: " << pos  << " - " << (uint8_t)axiom->at(pos) << std::endl;
            ++pos;
        }
        //std::cout << pos << "/"<< axiom->size() << "\r" << std::flush;
    }
    std::cout << "Finish replacement" << std::endl;
    return 0;
}

void Stats::analyse(std::string* seq){
    //std::cout << "Analysing..." << std::endl;
    //redundantPairs_->printPairs();
    //std::cout << "Analysing... start" << std::endl;
    uint64_t inputsize = (*seq).length();
    uint64_t i = 1;
    //std::cout << "Analysing... befor while" << std::endl;
    while (i < inputsize){

        char first = (*seq)[i-1];
        char last = (*seq)[i];
        std::string pair = "";
        pair.push_back(first);
        pair.push_back(last);

        addChar(first);
        addChar(last);

        //auto itr = redundantPairs_->find(pair);
        //std::cout << "Analysing... inside while at index "<< i << std::endl;
        redundantPairs_->insert(pair, i-1);
        
        ++i;
    }
    //std::cout << "Analysing... after while" << std::endl;

    //if(redundantPairs_->empty()){std::cout << "Pairs Empty!!!" << std::endl;}
    //redundantPairs_->printPairs();

    deleteOutdatedPairs();
    //std::cout << "Analysing... Axiom done" << std::endl;
}

int Stats::replaceredundantPairs(Grammar* cfg){
    int counter = 0;
    //int maxReplaces = ((nonterminals_->size() * currentIteration_) / iterations_) + 1; //TODO: Testing NT / (iterations - current)
    int maxReplaces = (nonterminals_->size() / (iterations_ - currentIteration_));
    
    if(maxReplaces == 0){
        changes = false;
    } else {
        changes = true;
    }

    //std::cout << "maxReplaces = " << maxReplaces << std::endl;
    //while(!redundantPairs_->empty()){
    while(counter < maxReplaces){
        std::string nt = "";
        if (getnextNt()){
            nt.push_back(nextNt_);
        } else {
            break;
        }
        //std::cout << "Search most occuring pair..." << std::flush;
        std::string pair = getMaxPair();
        //std::cout << "next Pair: " << +uint8_t(pair.at(0)) << " " << +uint8_t(pair.at(1)) << std::endl;
        auto occ = redundantPairs_->at(pair)->size(); 
        if(occ == 0){break;} 
        //std::cout << "done, found " << pair << " with " << occ << " occurrences" << std::endl;

        //auto start = std::chrono::duration_cast< std::chrono::milliseconds >(
        //    std::chrono::system_clock::now().time_since_epoch()
        //);

        replacePair(cfg, nt, pair);
        //add nonterminal here
        //std::cout << "replace done" << std::endl;

        //auto end = std::chrono::duration_cast< std::chrono::milliseconds >(
        //    std::chrono::system_clock::now().time_since_epoch()
        //);
        //std::cout << " took ms: " << (end - start).count() << std::endl;
        
        cleanupOccurrences(pair);

        deleteOutdatedPairs();
        
        //std::cout << "Pairs left: " << redundantPairs_->size() << std::endl;
        //end = std::chrono::duration_cast< std::chrono::milliseconds >(
        //    std::chrono::system_clock::now().time_since_epoch()
        //);
        //std::cout << "EndFunction took ms: " << (end - start).count() << std::endl;
        ++counter;
    }
    return 0;
}

void Stats::replacePair(Grammar* cfg, const std::string nt, const std::string pair){
    auto seq = cfg->getAxiomPtr();
    uint64_t occ = 0;

    uint64_t size = pair.size();
    //std::string replacer = "";
    //replacer = nt;
    //replacer.push_back(placeholder_);

    auto set = redundantPairs_->at(pair);
    auto end = set->end();

    for (auto itr = set->begin(); itr != end; ++itr){ //iterate through Occ list maybe end to start
        uint64_t index = *itr;
        auto endBlocked = index + 1;
        if(seq->at(index) != placeholder_ && seq->at(endBlocked) != placeholder_){
            seq->at(index) = nt[0];
            seq->at(index+1) = placeholder_;
            occ++;
        }
    }
    //cfg->setAxiom(seq);
    //std::cout << std::endl << "Store rule..." << pair << std::endl;
    auto rule = new Rule(nt, pair, occ);
    //std::cout << "add rule to grammar..." << pair << std::flush;
    cfg->addRule(rule);
    //std::cout << " done" << std::endl;
}

void Stats::cleanupOccurrences(std::string pair){
    //std::cout << "clean occurrences... " << std::flush;
    redundantPairs_->cleanupOccurrences(pair);
    //std::cout << "done" << std::endl;
}

void Stats::deleteOutdatedPairs(){
    //std::cout << "delete unneeded pairs... ";
    redundantPairs_->deleteOutdatedPairs();
    //std::cout << "done" << std::endl;
}

void Stats::addChar(uint8_t c){
    alphabet_->insert(c);
}

void Stats::populateNonTerminals(){
    uint8_t min = 0;
    uint8_t max = min - 1;

    for (auto itr = min; itr <= max; ++itr){
        if ( alphabet_->find(itr) == alphabet_->end()){
            nonterminals_->push_back(itr);
        }
        if ( itr == max ){break;}
    }
    nonterminalsSet_ = true;
}

bool Stats::getnextNt(){
    bool found = false;
    if ( nonterminals_->size() != 0){
        nextNt_ = *nonterminals_->rbegin();
        nonterminals_->pop_back();
        found = true;
    }

    return found;
}

void Stats::setIterations(uint16_t iterations){
    iterations_ = iterations;
}

void Stats::setCurrentIteration(uint16_t current){
    currentIteration_ = current;
}

std::string Stats::getMaxPair(){
    return redundantPairs_->getMaxPair();
}

uint64_t Stats::getalphabet_Size(){
    return alphabet_->size();
}

void Stats::printPairs(){
    //TODO: rewrite to print out file stats for analysis-mode

}

void Stats::printAlphabet(){
    std::set<uint8_t>::iterator itr;
    for (itr = alphabet_->begin(); itr != alphabet_->end(); ++itr) {
        std::cout << *itr << " ";
    }    
    std::cout << std::endl;
}
