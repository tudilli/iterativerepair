#pragma once

#include <string>
#include <map>
#include <fstream>
#include <filesystem>
#include "Rule.hpp"

class Grammar{
    private:
        std::string axiom_;
        uint8_t ruleseperator_;
        std::map<std::string, Rule*>* rules_; //testing the sorting criteria with std::greater<int>

    public:
        Grammar(std::string);
        void addRule(Rule*);
        void deleteRule(std::string);
        void setAxiom(std::string);
        void setRuleseperator(uint8_t);
        std::string* getAxiomPtr();
        std::map<std::string, Rule*>* getRulesPtr();
        uint64_t getNumberOfRules();
        void cleanupAxiom(const char);
        void convertToSLP();
        std::string toString();
        std::string rulesToString();
        int writeToCompressedFile(const std::string&);
        int writeToDecompressedFile(const std::string&);
        ~Grammar();
};