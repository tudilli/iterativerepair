﻿// RePairMetric.h: Includedatei für Include-Standardsystemdateien
// oder projektspezifische Includedateien.

#pragma once

#include <iostream>
#include <fstream>
#include <sstream>

#include "Stats.hpp"
#include "Grammar.hpp"

void analyse(std::string);
int compress(std::string, std::string, uint16_t);
int decompress(std::string, std::string, uint8_t);
std::string readFileIntoString(const std::string&);
