#pragma once

#include <string>
#include <iostream>

class Rule{
    private:
        std::string nonterminal_;
        std::string target_;
        uint64_t uses_;
        int64_t improvement_;

        int64_t calculateImprovement();

    public:
        Rule(std::string, std::string, uint64_t);
        ~Rule();
        std::string getTarget();
        std::string* getTargetPtr();
        std::string getNonterminal();
        int64_t getImprovement();
};
