#pragma once

#include <set>
#include <map>
#include <vector>
#include <string>
//#include <algorithm>
//#include <boost/algorithm/string/replace.hpp>

#include "Grammar.hpp"
#include "PairsContainer.hpp"
#include <chrono>
//#include "Rule.hpp"

class Stats{
    private:

        //------Members
        std::set<uint8_t>* alphabet_;
        std::vector<uint8_t>* nonterminals_;
        bool nonterminalsSet_;
        uint8_t nextNt_;
        PairsContainer* redundantPairs_;
        //std::set<uint64_t>* blockedIndecies_;
        uint8_t placeholder_;
        uint16_t iterations_;
        uint16_t currentIteration_;
        bool changes;

        void addChar(uint8_t);
        void addMapItem(std::string, uint64_t);
        void cleanupOccurrences(std::string);
        void deleteOutdatedPairs();
        void populateNonTerminals();
        bool getnextNt();
        std::string getMaxPair();
        int replaceredundantPairs(Grammar*);
        void replacePair(Grammar* , const std::string, const std::string);

    public:
        Stats();
        Stats( const Stats& );
        ~Stats();

        void analyse(std::string*);
        int compress(Grammar*);
        int decompress(Grammar*, uint8_t);
        
        uint64_t  getalphabet_Size();
        void setIterations(uint16_t);
        void setCurrentIteration(uint16_t);
        void printPairs();
        void printAlphabet();
};
