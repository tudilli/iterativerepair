#include "PairsContainer.hpp"

//-------Methods
PairsContainer::PairsContainer(uint64_t size){
    size_ = size;
    pairsOcc_ = new std::vector<std::vector<uint64_t>*>(size);
    for (uint64_t itr = 0; itr < size; ++itr){
        pairsOcc_->at(itr) = new std::vector<uint64_t>();
    }
}

PairsContainer::~PairsContainer(){
    for (uint64_t itr = 0; itr < size_; ++itr){
        delete pairsOcc_->at(itr);
    }
    delete pairsOcc_;
}

uint64_t PairsContainer::size(){
    uint64_t entries = 0;
    for (uint64_t itr = 0; itr < size_; ++itr){
        if (pairsOcc_->at(itr)->size() > 4){++entries;}
    }
    return entries;
}

std::vector<uint64_t>* PairsContainer::find(std::string key){
    return pairsOcc_->at(keyToIndex(key));
}

void PairsContainer::insert(uint64_t pair, uint64_t index){
    if(pair >= size_){std::cout << "unexpected pair" << pair << std::endl;}
    auto entry = pairsOcc_->at(pair); 
    entry->push_back(index);
}

void PairsContainer::insert(std::string pair, uint64_t index){
    insert(keyToIndex(pair), index);
}

std::vector<uint64_t>* PairsContainer::at(uint64_t index){
    return pairsOcc_->at(index);
}

std::vector<uint64_t>* PairsContainer::at(std::string key){
    return pairsOcc_->at(keyToIndex(key));
}

bool PairsContainer::empty(){
    for(uint64_t itr = 0; itr < size_; ++itr){
        if(!(pairsOcc_->at(itr)->size() > 4)){
            return false;
        }
    }
    return true;
}

void PairsContainer::erase(std::string key){
    pairsOcc_->at(keyToIndex(key))->clear();
}

uint64_t PairsContainer::keyToIndex(std::string key){
    auto length = key.size();
    if(length != 2){std::cout << "unexpected key length" << length << std::endl;}
    uint64_t index = 0;
    if(index >= 0xffff){std::cout << "unexpected index 1 outside" << index << std::endl;}
    index = index + (uint8_t)key[0];
    if(index >= 0xffff){std::cout << "unexpected index 2 outside" << index << std::endl;}
    for (std::size_t itr = 1; itr < length; ++itr){
        if(index >= 0xffff){std::cout << "unexpected index 1 inside" << index << std::endl;}
        index = index << 8;
        if(index >= 0xffff){std::cout << "unexpected index 2 inside" << index << std::endl;}
        index = index + (uint8_t)key[itr];
        if(index >= 0xffff){std::cout << "unexpected index 3 inside " << index << " itr: "<< itr << " charAtItr: " << (uint64_t)key[itr] << std::endl;}
    }
    if(index >= 0xffff){std::cout << "unexpected index " << index << "(" << key << ")" << std::endl;}
    return index;
}

std::string PairsContainer::indexToKey(uint64_t index){
    std::string out = "";
    uint8_t firstChar;
    uint8_t secondChar;

    firstChar = (index & 0xff00) >> 8;
    secondChar = (index & 0x00ff);

    out.push_back(firstChar);
    out.push_back(secondChar);

    return out;
}

bool PairsContainer::doOverlap(uint64_t index, std::string pair){
    bool overlaping = false;
    std::string key = indexToKey(index);

    if (key[0] == pair[1] || key[1] == pair[0]){
        overlaping = true;
    }

    return overlaping;
}

std::vector<std::vector<uint64_t>*>* PairsContainer::getPairsOcc(){
    return pairsOcc_;
}

void PairsContainer::erase(std::string key, uint64_t index){
    auto entry = find(key);
    //uint64_t location = 0;

    for (auto itr = entry->begin(); itr!=entry->end(); ++itr){
        if(*itr == index){
            //location = *itr;
            entry->erase(itr);
            break;
        }
        if(*itr > index){
            break;
        }
    }
}

std::string PairsContainer::getMaxPair(){
    uint64_t index = 0;
    for (uint64_t i = 1; i < size_; ++i){
        if (pairsOcc_->at(i)->size() > pairsOcc_->at(index)->size()){
            index = i;
        }
    }

    return indexToKey(index);
}

void PairsContainer::deleteOutdatedPairs(uint64_t minimum){
    std::set<uint64_t> toDelete;
    for (uint64_t itr = 0; itr < size_; ++itr) {
        if( pairsOcc_->at(itr)->size() < minimum){
            toDelete.insert(itr);
        }
    }

    auto endDelete = toDelete.end();
    for (auto itr = toDelete.begin(); itr != endDelete; ++itr){
        pairsOcc_->at(*itr)->clear();
    }
}

void PairsContainer::deleteOutdatedPairs(){
    deleteOutdatedPairs(4);
}

void PairsContainer::cleanupOccurrences(std::string pair){
    std::vector<std::string> candidates;

    pairsOcc_->at(keyToIndex(pair))->clear();
    for (uint64_t itr = 0; itr != size_; ++itr){
        if( !(pairsOcc_->at(itr)->empty()) && doOverlap(itr, pair)){
            candidates.push_back(indexToKey(itr));
        }
    }

    //std::cout << "#candidates: " << candidates.size() << " " << std::flush;

    auto end = candidates.end();

    for (auto pairItr = candidates.begin(); pairItr != end; ++pairItr) {
        erase(*pairItr);
    }
}

void PairsContainer::printPairs(){
    for (uint64_t itr = 0; itr < size_; ++itr){
        if (! (pairsOcc_->at(itr)->empty())){
            std::cout << indexToKey(itr) << "(" << itr << "): " << pairsOcc_->at(itr)->size() << " " << std::flush;
        }
    }
    std::cout << std::endl;

    std::cout << "ContainerSize: " << size_ << std::endl;
}