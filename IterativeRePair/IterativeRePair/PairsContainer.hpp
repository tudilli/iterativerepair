#pragma once

#include <vector>
#include <string>
#include <set>
#include <iostream>
//#include <cstdint>

class PairsContainer{
    private:
        //-------Members
        uint64_t size_;
        std::vector<std::vector<uint64_t>*>* pairsOcc_;

        //-------Methods
        uint64_t keyToIndex(std::string key);
        std::string indexToKey(uint64_t index);
        bool doOverlap(uint64_t index, std::string pair);

    public:
        PairsContainer(uint64_t size);
        ~PairsContainer();
        uint64_t size();
        std::vector<uint64_t>* find(std::string key);
        void insert(uint64_t pair, uint64_t index);
        void insert(std::string pair, uint64_t index);
        std::vector<uint64_t>* at(uint64_t key);
        std::vector<uint64_t>* at(std::string key);
        std::vector<std::vector<uint64_t>*>* getPairsOcc();
        bool empty();
        void erase(std::string key);
        void erase(std::string key, uint64_t index);
        std::string getMaxPair();
        void deleteOutdatedPairs(uint64_t minimum);
        void deleteOutdatedPairs();
        void cleanupOccurrences(std::string pair);
        void printPairs();
};